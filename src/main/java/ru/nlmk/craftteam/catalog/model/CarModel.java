package ru.nlmk.craftteam.catalog.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "car_model")
public class CarModel implements Serializable {
    @Id
    private Integer id;

    @Column
    private String name;

    @Column(name = "brand_id")
    private Integer brandId;
}
