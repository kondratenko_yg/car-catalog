package ru.nlmk.craftteam.catalog.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "car_brand")
public class CarBrand implements Serializable {
    @Id
    private Integer id;

    @Column
    private String name;
}
