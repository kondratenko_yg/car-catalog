package ru.nlmk.craftteam.catalog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.nlmk.craftteam.catalog.model.CarModel;

import java.util.List;


public interface CarModelRepository extends JpaRepository<CarModel, Integer> {
    List<CarModel> findAllByBrandId(Integer brandId);
}
