package ru.nlmk.craftteam.catalog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nlmk.craftteam.catalog.model.CarBrand;


public interface CarBrandRepository extends JpaRepository<CarBrand, Integer> {
}
