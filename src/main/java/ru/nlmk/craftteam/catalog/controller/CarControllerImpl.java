package ru.nlmk.craftteam.catalog.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nlmk.craftteam.catalog.model.CarBrand;
import ru.nlmk.craftteam.catalog.model.CarModel;
import ru.nlmk.craftteam.catalog.repository.CarBrandRepository;
import ru.nlmk.craftteam.catalog.repository.CarModelRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class CarControllerImpl implements  CarController{

    private CarBrandRepository carBrandRepository;

    private CarModelRepository carModelRepository;

    @Autowired
    public CarControllerImpl(CarBrandRepository carBrandRepository, CarModelRepository carModelRepository) {
        this.carBrandRepository = carBrandRepository;
        this.carModelRepository = carModelRepository;
    }

    @Override
    @ApiOperation("Возвращает список марок автомобилей")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "Марки найдены"),
            @ApiResponse(code=404,message = "Не найдены")
    })
    @GetMapping(value = "/brands", produces = "application/json")
    public List<CarBrand> getAllBrands() {
        return carBrandRepository.findAll();
    }

    @Override
    @ApiOperation("Возвращает модели автомобилей заданной марки")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "Модели найдены"),
            @ApiResponse(code=404,message = "Марка Не найдена")
    })
    @GetMapping(value = "/models/{brandId}", produces = "application/json")
    public List<CarModel> getModelsByBrandId(@PathVariable Integer brandId) {
        return carModelRepository.findAllByBrandId(brandId);
    }
}
