package ru.nlmk.craftteam.catalog.controller;



import ru.nlmk.craftteam.catalog.model.CarBrand;
import ru.nlmk.craftteam.catalog.model.CarModel;

import java.util.List;

public interface CarController {
    List<CarBrand>  getAllBrands();
    List<CarModel>  getModelsByBrandId(Integer id);
}
